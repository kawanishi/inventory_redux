export const addStock = text => ({
  type: "ADD_STOCK",
  text
})

export const increment = payload => ({
  type: "INCREMENT",
  payload
})

export const decrement = payload => ({
  type: "DECREMENT",
  payload
})

export const deleteStock = payload => ({
  type: "DELETE_STOCK",
  payload
})
