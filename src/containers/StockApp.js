import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addStock } from "../actions";
import AddStock from "../components/AddStock";
import StockList from "../components/StockList";

class StockApp extends React.Component {
  render() {
    return (
      <div>
        <AddStock onSubmit={this.props.onSubmit} />
        <StockList data={this.props.data} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.stocks
});

const mapDispatchToProps = dispatch => ({
  onSubmit: text => {
    dispatch(addStock(text));
  }
});

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(StockApp);
export default AppContainer;
