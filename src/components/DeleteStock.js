import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteStock } from "../actions";

class DeleteStock extends React.Component {
  render() {
    const { stock } = this.props;
    return (
      <span>
        <button onClick={() => this.props.deleteStock(stock)}>削除</button>
      </span>
    )
  }
}

const mapStateToProps = state => {};

const mapDispatchToProps = dispatch => ({
  deleteStock: stock => {
    dispatch(deleteStock(stock));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteStock);
