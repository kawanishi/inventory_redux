import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { increment, decrement } from "../actions";

class ChangeQuantity extends React.Component {
  render() {
    const { stock } = this.props;
    return (
      <span>
        <button onClick={() => this.props.increment(stock)}>+</button>
        &nbsp;
        <button onClick={() => this.props.decrement(stock)}>-</button>
        &nbsp;
        <strong>{stock.quantity} 個</strong>
      </span>
    );
  }
}

const mapStateToProps = state => {};

const mapDispatchToProps = dispatch => ({
  increment: stock => {
    dispatch(increment(stock));
  },
  decrement: stock => {
    dispatch(decrement(stock));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeQuantity);
