import React from "react";
import PropTypes from "prop-types";
import ChangeQuantity from "../components/ChangeQuantity";
import DeleteStock from "../components/DeleteStock";

class StockList extends React.Component {
  render() {
    return (
      <div>
        {this.props.data.map(stock => (
          <div>
            {stock && stock.name} <ChangeQuantity stock={stock} /> <DeleteStock stock={stock} />
          </div>
        ))}
      </div>
    );
  }
}

export default StockList;
