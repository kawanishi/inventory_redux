import React from "react";
import PropTypes from "prop-types";

class AddStock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newItem: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleClick(event) {
    event.preventDefault();

    const { newItem } = this.state;
    if (newItem && newItem.length > 0) {
      this.props.onSubmit(newItem);
      this.setState({ newItem: "" });
    }
  }

  handleChange(event) {
    const newItem = event.target && event.target.value;
    this.setState({ newItem: newItem.trim() });
  }

  render() {
    const { newItem } = this.state;
    return (
      <form>
        <input type="text" onChange={this.handleChange} value={newItem} />
        <button onClick={this.handleClick}>登録</button>
      </form>
    );
  }
}

export default AddStock;
