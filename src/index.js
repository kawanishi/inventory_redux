import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers'
import StockApp from './containers/StockApp'

const store = createStore(rootReducer)

render(
  <Provider store={store}>
    <StockApp />
  </Provider>,
  document.getElementById('root')
)
