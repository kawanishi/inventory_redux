const initialState = [];

let nextId = 0;

const stocks = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_STOCK":
      const newItem = {
        id: nextId++,
        name: action.text,
        quantity: 0
      };
      return [...state, newItem];
    case "INCREMENT":
      return state.map(stock => {
        return stock.id === action.payload.id
          ? Object.assign({}, stock, { quantity: stock.quantity + 1 })
          : stock;
      });
    case "DECREMENT":
      return state.map(stock => {
        return stock.id === action.payload.id && stock.quantity > 0
          ? Object.assign({}, stock, { quantity: stock.quantity - 1 })
          : stock;
      });
    case "DELETE_STOCK":
      return state.filter((stock) => stock.id !== action.payload.id);
    default:
      return state;
  }
};

export default stocks;
